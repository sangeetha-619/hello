import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
//import { Product, products } from '../products';
import { provideRoutes } from '@angular/router';

@Component({
  selector: 'app-employeedetails',
  templateUrl: './employeedetails.component.html',
  styleUrls: ['./employeedetails.component.css']
  
})
export class EmployeedetailsComponent implements OnInit {
  itemsReceived1: string[] = [];
  itemsReceived2: string[] = [];
  itemsReceived3: string[] = [];

  getitemsFromService1(){
    this.itemsReceived1 = this.dataService.getItems1()
  }
  getitemsFromService2(){
    this.itemsReceived2 = this.dataService.getItems2()
  }
  getitemsFromService3(){
    this.itemsReceived3 = this.dataService.getItems3()
  }




  constructor(
    private dataService: DataService

  ) { }

  ngOnInit(): void {
  }

}
