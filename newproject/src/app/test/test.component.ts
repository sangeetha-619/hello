import { Component, OnInit } from '@angular/core';
import {ModelService} from '../model.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  constructor(public modelservice: ModelService) { }

  modelList: any;

  ngOnInit(): void {
     this.modelservice.getmodel().subscribe(data =>  {
      this.modelList = data;
    });
  }

  

}
