import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ModelService {

  constructor(private httpClient: HttpClient) { }
  getmodel(){
    return this.httpClient.get('https://livescore6.p.rapidapi.com/matches/v2/list-live');
  }
  callingFromTemplate(){
    console.log("Calling From Template Directly")
  }
}
