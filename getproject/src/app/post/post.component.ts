import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  postId: any;

  constructor(private http: HttpClient) { }


  ngOnInit(): void {

    this.http.post<any>('https://jsonplaceholder.typicode.com/posts', { name: 'Angular POST Request ' }).subscribe(data => {
      this.postId = data.id;
  })

  }

}
