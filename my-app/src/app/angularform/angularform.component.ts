import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import {DataService} from '../data.service';

@Component({
  selector: 'app-angularform',
  templateUrl: './angularform.component.html',
  styleUrls: ['./angularform.component.css']
})
export class AngularformComponent  {

  constructor(
    private formBuilder: FormBuilder,
    public dataService: DataService,
  ) { }
  
  
    
  

  checkoutForm = this.formBuilder.group({
    firstname: '',
    middlename: '',
    lastname: '',
    email: '',
    phone:''


  });

 
  onSubmit(): void {
    window.alert('please enter all the fields');
    console.log('Your details', this.checkoutForm.value);
    this. dataService. postList(this.checkoutForm.value.subscribe())
    this.checkoutForm.reset();
   

    };
}
