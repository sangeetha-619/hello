import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-myform',
  templateUrl: './myform.component.html',
  styleUrls: ['./myform.component.css']
})
export class MyformComponent{

  constructor(
    private formBuilder: FormBuilder,
  ) { }
 
  checkoutForm = this.formBuilder.group({
    firstname: '',
    middlename: '',
    lastname: ''
  });
 
  onSubmit(): void {
    window.alert('please enter all the fields');
    console.log('Your details', this.checkoutForm.value);
    this.checkoutForm.reset();
   

  }
}
