import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
//import { MyformComponent } from './myform/myform.component';
import { AngularformComponent } from './angularform/angularform.component';
@NgModule({
  declarations: [
    AppComponent,
   // MyformComponent,
    AngularformComponent,
   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
